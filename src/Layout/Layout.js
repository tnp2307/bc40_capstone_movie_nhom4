import React, { Component } from "react";
import Footer from "../Component/Footer/Footer";
import Header from "../Component/Header/Header";

export default function Layout({ Component }) {
  return (
    <div id="main-bg" >
      <Header />
      <Component/>
      <Footer />
    </div>
  );
}
