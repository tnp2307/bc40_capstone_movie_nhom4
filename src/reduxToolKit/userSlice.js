import { createSlice } from "@reduxjs/toolkit";
import { localStore } from "../service/localUserStorage";

const initialState = {
  userInfor: localStore.get(),
  selectedMovie: null,
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setLoginAction: (state, action) => {
      state.userInfor = action.payload;
    },
    setSelectMovieAction: (state, action) => {
      state.selectedMovie = action.payload;
    }
  },
});

export const {setLoginAction, setSelectMovieAction} = userSlice.actions;

export default userSlice.reducer;
