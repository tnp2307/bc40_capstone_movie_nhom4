import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  selectedSeatList: [],
};

const selectMovieSlice = createSlice({
  name: "selectMovieSlice",
  initialState,
  reducers: {
    setSelectMovieAction: (state, action) => {
      let cloneSeatList = [...state.selectedSeatList];
      let index = cloneSeatList.findIndex(
        (seat) => seat.maGhe == action.payload.maGhe
      );
      if (index == -1) {
        cloneSeatList.push(action.payload);
      
      } else {
        cloneSeatList.splice(index, 1);
    
      }
      state.selectedSeatList = cloneSeatList;
    },
  },
});

export const { setSelectMovieAction } = selectMovieSlice.actions;

export default selectMovieSlice.reducer;
