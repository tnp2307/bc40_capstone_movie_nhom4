import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  headerInfor: {
    gp: "",
    title: "",
    img: "",
  }
}

export const groupInfor = [
  {
    gp: "06",
    img: "hot",
    title: "Now showing"
  },
  {
    gp: "09",
    img: "blockbuster",
    title: "Blockbuster"
  }
]

const selectTypeMovieSlice = createSlice({
  name: "selectMovieTypeSlice",
  initialState,
  reducers: {
    setMovieList: (state, action) => {
      state.headerInfor.gp = action.payload.gp
      state.headerInfor.title = action.payload.title
      state.headerInfor.img = action.payload.img
    }
  }
});

export const { setMovieList } = selectTypeMovieSlice.actions

export default selectTypeMovieSlice.reducer