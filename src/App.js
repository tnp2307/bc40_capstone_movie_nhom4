import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";
import SignupPage from "./Pages/SignupPage/SignupPage";
import HomePage from "./Pages/HomePage/HomePage";
import Layout from "./Layout/Layout";
import MovieDetail from "./Pages/MovieDetail/MovieDetail";
import ModalVideo from "react-modal-video";
import Spinner from "./Component/Spinner/Spinner";
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import MovieList from "./Pages/HomePage/MovieList/MovieList";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCu1FoGWYMavB_fX4pVBye6QUqPeskzJJ4",
  authDomain: "vie-cine-system.firebaseapp.com",
  projectId: "vie-cine-system",
  storageBucket: "vie-cine-system.appspot.com",
  messagingSenderId: "694977680425",
  appId: "1:694977680425:web:0bf58e759d8a0e28837838",
  measurementId: "G-19EWLQJC8J"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route path="/sign-up" element={<Layout Component={SignupPage} />} />
          <Route path="/homepage" element={<Layout Component={HomePage} />} />
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={MovieDetail} />}
          />
          <Route
            path="/movie-list/:id"
            element={<Layout Component={MovieList} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
