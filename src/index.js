import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./reduxToolKit/userSlice";
import movieModalSlice from "./reduxToolKit/movieModalSlice";
import spinnerSlice from "./reduxToolKit/spinnerSlice";
import selectMovieSlice from "./reduxToolKit/selectMovieSlice";
import SelectTypeMovieSlice from "./reduxToolKit/SelectTypeMovieSlice";


const root = ReactDOM.createRoot(document.getElementById("root"));
const store = configureStore({
  reducer: {
    userSlice,
    movieModalSlice,
    spinnerSlice,
    selectMovieSlice,
    SelectTypeMovieSlice,
  },
});
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

reportWebVitals();
