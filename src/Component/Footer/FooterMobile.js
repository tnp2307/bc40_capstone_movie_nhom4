import React from "react";
import logo from "../../resource/logo.png"
export default function FooterMobile() {
  return (
    <div id="footer" className=" py-12 ">
      <div className="container flex flex-row justify-between ">
        <div className="flex flex-col w-1/2 ">
          <div className="img-logo mb-5"><img src={logo} width={372} height={83} /></div>
        
          <div className="text-white ">
            <h2 className="mb-3 font-bold">
              Công Ty Cổ Phần Dịch Vụ Di Động Trực Tuyến Viecine
            </h2>
            <p className="font-extralight font">
              Lầu 6, Toà nhà Phú Mỹ Hưng, số 8 Hoàng Văn Thái, Tân Phú, Quận 7,
              TpHồ Chí Minh
            </p>
          </div>
        </div>
        <div className=" w-1/3 flex flex-row justify-between text-white">
          <div className="flex flex-col">
            <h2 className="  mb-3 font-bold">Social Media</h2>
            <div className="logo">
              <i class="fab fa-instagram"></i>
              <i class="fab fa-linkedin-in"></i>
              <i class="fab fa-facebook-f"></i>
              <i class="fab fa-twitter"></i>
            </div>
          </div>
          <div className="flex flex-col">
            <h2 className="mb-3 font-bold">Other</h2>
            <p className="font-extralight font">
            Terms & Policies
            </p>
            <p className="font-extralight font">
            Sitemap
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
