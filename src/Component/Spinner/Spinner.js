import React from "react";
import { useSelector } from "react-redux";
import { DotLoader } from "react-spinners";

export default function Spinner() {
  const { isLoading } = useSelector((state) => state.spinnerSlice);
  // let isLoading = true
  return isLoading ? (
    <div className="fixed flex h-screen w-screen bg-slate-500 opacity-30 z-50 justify-center items-center">
      <DotLoader color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}
