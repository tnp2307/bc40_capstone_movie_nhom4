import { Button } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { setCloseModal } from "../../reduxToolKit/movieModalSlice";

export default function CloseButton() {
  let dispatch = useDispatch();
  let setClose = () => {
    dispatch(setCloseModal());
  };
  return <Button danger className="absolute right-60 top-60  flex justify-center items-center z-20 " onClick={setClose}>X</Button>;
}
