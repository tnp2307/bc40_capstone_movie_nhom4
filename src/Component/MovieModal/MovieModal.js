import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ModalVideo from "react-modal-video";
import CloseButton from "./CloseButton";
import { setCloseModal } from "../../reduxToolKit/movieModalSlice";
import 'react-modal-video/scss/modal-video.scss';

export default function MovieModal() {
  let dispatch = useDispatch();
  let setClose = () => {
    dispatch(setCloseModal());
  };
  let { isOpen,url } = useSelector((state) => state.movieModalSlice);
  let getId = (url) => {
    const regExp =
      /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return match && match[2].length === 11 ? match[2] : null;
  };
  let videoId=""
  if (url){
     videoId = getId(url);
  }
 
  return isOpen ? (
      <ModalVideo
        channel="youtube"
        autoplay
        isOpen={isOpen}
        videoId={videoId}
        onClose={() => setClose()}
      />
  ) : (
    <></>
  );
}
