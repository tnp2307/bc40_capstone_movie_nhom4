import React, { useState } from 'react';
import {  Drawer } from 'antd';
import { NavLink } from "react-router-dom";
import { Link } from "react-scroll";
import UserMenuTablet from "./UserMenuTablet";
import SearchBar from "./SearchBar";

export default function HeaderMobile() {
  const [open, setOpen] = useState(false);
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  return (
    <div className="bg-black fixed w-full z-50 ">
      <div className="bg-black flex justify-between items-center container">
        <nav class="bg-black text-white border-gray-200 dark:bg-gray-900 dark:border-gray-700 ">
          <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4 space-x-2">
            <NavLink to="/homepage" class="flex items-center">
              <svg
                width={143}
                height={41}
                viewBox="0 0 143 41"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
              >
                <rect
                  y="0.5"
                  width="142.857"
                  height={40}
                  fill="url(#pattern0)"
                />
                <defs>
                  <pattern
                    id="pattern0"
                    patternContentUnits="objectBoundingBox"
                    width={1}
                    height={1}
                  >
                    <use
                      xlinkHref="#image0_95_3961"
                      transform="matrix(0.000934579 0 0 0.00333778 0 -0.000667572)"
                    />
                  </pattern>
                  <image
                    id="image0_95_3961"
                    width={1070}
                    height={300}
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABC4AAAEsCAYAAAAM3/IKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAKcFJREFUeNrs3d1128bWMGDwXbn/dCoIfae7yBWYrCBKBZEqsF2BpQrsVGClAisVSKnAyp3vzFRwdCrghxGGtiTrh5RIYg/medbikp0fEQQ2B3s2NgZNAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVG9kFwFPNd8/22x977etV/kfj9nWZX8nf7et89GV6bm8BbGAcns/TuJvG4l/yGNzkcfki/3n2bSwejWb2GAAlUrgAVkuSd89SYvwuJ8o7S/5vqZDxR/v6MPoyvbQXAZ45Fs/nB+2P101XpFjWefs6Ho1G5/YgACVRuACWT5R3z45yorzzxF+RihaHoy/TU3sT4Anj8HyeChXv29fkGb/m9GosHo0UkgEogsIF8HiivHuWChWfnpkoX3cy+jI9tGcBVhiLuy6LVLTYWcOvS0WL6Wg0urBnAYhO4QJ4OFHuihZnzWrtyMtIa19M7WGAJcbirmjxcc2/tuuCG410wQEQmsIFcH+ivLmixYLOC4DHxuLNFC0WdF4AEN7/2QXAA1JL8t4Gf/9BXjcDgDtcW9NiU64K1O377NjbAESl4wK4O1nePZs0XbfFNrwYfZnO7HWAW2PxfJ7G4ckW3upkNBrpgAMgJB0XwH0+DvS9AIqQbxGZbOntDtr3m9jrAESkcAH8mCzvnu23P8ZbfMtJ+55jex7ghtdbfr/f7XIAIlK4AKIkr6/tdoBOXttib8tve2CtCwAiUrgA7jKp5D0BjMPGYgCCU7gAbpjvnqUrfH1ccduz9wG+edXT+xqLAQhH4QK4rbc24Vw0AaC/sfgXux6AaBQugNvGFSbqAMZi4zAAQSlcAFGSZQCMxQDwA4UL4LaZXQDQuwu7AAA6ChfAbbNK3xsgkkvjMAB0FC6AMEnr6MtUwgzQ71j8r10PQDQKF8ANuXjQx5W+c3sf4Jt/enpfYzEA4ShcAHc57eE9/7LbAXodhy9Ho9G5XQ9ANAoXwF3+rCRJBwhpNBrNmu13PxiHAQhJ4QL4MWH+Mj3fcsJ8Yn0LgB8cD/z9AGApChdA3wnspWQZ4Ef5to3zLb3dh9zlAQDhKFwAdyfMXdfFhy281VvdFgD3Omw2v2DyRaOADEDkuYldADxkvnt21v6YbOjXp1tEDu1lgAfG4fk8jcFnG/r1qSgyHY1GF/Y0AFHpuAAe81vTXY1bN0ULgCXkW0Y2MV4qWgBQxrnQLgCWMd89+9j+OFjTrzsefZke2asAK4zD8/le03Ve7Kzh16VixaGiBQAl0HEBLCV3R6Tui+fcaz1r0tU9RQuA1cfhrsjwonn+Y0s/NDotACjpHGgXAKuY756lK30H7et1+xov+b+l5PiP0ZfpiT0IsIaxuFv34vdm+U64VHROBY9jTw8BoDQKF8DTE+fds9S2nJLnn9vX3q1/nYoV/7Svc08NAdjQODyf7+RxOI3BvzQ3byO5/DYOd+tkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfDeyC+KY756N2x/jYJt1MfoyvXR0Qh+nWXuMZgP5fOJt/fE66TOe4CoO5/NV4/ByNBpd2Ge9qH7fb+BYrnxebY/BuT1H33F4lZeNRvIyQvjJLgjnLNj2HLevI4flh2M0DrQ90zTZXOPvO2hf73r8LJK1VRKR3bO9HI/p5y/tayf/eecZv/NbwpImMfnnv/mn4hJ3JcTX4/Dn/OenJMnXf+ddcfi/PEbUkEz3lQ+c57GY5WN1Me4u4n/v2lj83Pi/vPYd+Cf/eaa4xCNxmP78Kv+ryTN/711xOMvjsDhka3RcxJuEfMwTxyjSAPXCROXb8UnH5mOgTTpvj810zZ/xqM/CRft5FC7uPzY7OQF5lROTSU+bMssJzN85BiUu9SXHkxyDr8LE4cAS6Pm1mevWzyujkcLFw8dm79ZYPO7rWN36DsjV6ozDX/JPccig6biIJ3U4HATanpSg7revE4fmyruA8cKQE5OuoyJ9B39tnnH1bs3G+bWft/EyJy5/ta9Thc7BJsix43A+vxmHkmfWF/+LXOhV/rkTZNMm+fUmb+dF/g786Uq4OBSHDI2Oi5gTlWhdF+me9xeOy/C7LfLnPGp0XEQoVvyeE5NxgR/htFHEGEqxouQ4TAnzH02hRQwdF2Emib/mn6WZ5bHY5FEc9ukyx+Ef4pDnUriIOWlJCeLXYJt12E5ATio/Ll+DJe8bmeQrXPQWXyk5OcgTxb0BfbSTq8RZQaq0JPn1IOOwoAUPFS562++T5nvBbmcgH2vWdEW8E51IxcThXh6HxSFk/2cXxJNX9I9WJHhX8zHJ3RbjQJt0biI4mNga5y6rVBh7P7DJYpK+O2ep8Je/R8RMksft632Ow4+DjcP5/Gv7OsgFGrj+HUhx8bnpFkU9GNBkscn5S/p+/7f9jB/z0yWIG4cpBj9XEId7jjirULiIK9raBePKJx3WtmC9ycnNgsXQkpP7EpaPuYBxlDtM6D9JTgWLRRy+qSUO0+dtP/eRAgZ5ojjUgt1dDnL8f+rxkbs8HIeTSuLwcyrSiEOWpXARlK6LUBPMg0a3BeuLp9sFi9qM81iiA6PfJPl6waLG47DzLQ7n8yMRUf1EcVzhLki3IJzlAsZYRIjDnkxyHJ7pwOAxChex6bqIQbcFz09Ods922tf7iieKd00cFx0Y+3bH1pLknTxRF4ff4/Dd4hYSu6OK78Ck8onibWn8/Zpb93UgbTcOP4vDbyZN14HhVibupXARmK6LEJPNSaPbgufH0UHzvRWfm9L361O7j87ywsRsLlHez3H4zt64Mw4/uuo36PhPXUafmm4NC2PNj67OU+0+cp7abBzuXItDY83dcfhZJxx3UbiIT9dFv3Rb8PQEpbstJCUn6YqKK1kPm1wlK91TbVj/hC3F4SdxuGQcSpqH9h1Ik/F0dVt318PS+PBeAW+jcfhVHC4Vh6kT7rM45DqFi+B0XfQ66Zw0sRZI0m1RVvwsEuWJvbFisrJ7lgoYkpX1TtjE4YrnOUnzIOJ/UbR73yjarSKNFwp44rBve+KQ6xQuyqDroqfEVRywcoLSrWUhQXl+snKWiz88LVFetCOLw+cnzeKwzO9AylMU7Z6ZB+Xui7Fd8eQ43BeHa4nDz+IQhYsCBO26eD3wyeek0W3B0+LmqwRlLbqW5d2zTx6dunKivNdoi1+n9/nJC+KwjPjfyU/McYveeqTz2ec8AWe1WEyFY7forceeOEThohzRrrbv5UnaUOm2YLUEpesOOJOgrF33yD63jiybKB80XdFibG+sPQ7dOhI//sd5HD6wN9Yqndc+adlfOg538hNDdGttJg7f2xV1UrgohLUutjoBnTS6LVg+XtKtIenKnhPp5ixuHXGl5eFkeXGVmc24mhR7bGrY+E/n7c+NJzVsNO/TffRoHKb4+yoON+pNvoVJHFZG4aIs0a66TwbadaHbguUSlO4WBlf3tqO70lLXU42WTZR38sJv9s124vCjdS/CfQcOGh1v29J1wZk0isOe5yA5Dsd2RT0ULgqi62IrE9FJo9uC5WJlr/Ec9j58bPe97pbvifKieDaxN7bqfe5wof/vwFGj02jbrroK3Dp1Iw4PGuuq9BGHbuGriMJFeXRdbJZuCx5PUBQt+vYm355Te6KsJblfB4oXvX8HPjaVPKI9oKuiqUnjt0U4jQXikA1TuCiMrouNTkYnjW4LHo+TRdHCVZW+J40VFy9ykiYOI8RhO3nWNt/LdyB9/w/sCZPGAHHo1rEYcWgdrIFTuCiTrovN0G3BwwmKokW8SWOFxQtFi3hx2Lja2sdk8cCeCDVp3BOH9ByHnyyePGwKFwXSdbGRCemk0W3BwzFishh00lhT8ULRIqTLRqHZZNGksarihTgM673bRoZL4aJcui7WS7cF9ycoihbRVVG8yKuni8NYUtFiOhqNLuwKk8XKVVO8yGtaiENxyJYpXBRK18VaJ6WTRrcF98fHjsliEQ6G/LSRvIbCJ3EYiqLFdr8Db0wWi5k0jgcchykGrWlRRhwqXgyMwkXZInZdlDhI6Lbg7gRF0aI06Wkjg5vYXHvkqSQsDkWL7U8WPQa5nEnjpyEuWHvtkaeIQ3qgcFGwoF0XrwubmE4a3Rbc76PJYnnHbGCPaG7yhE0cxqFosd3J4p7JYnEWt1cOLQ4Vz8oybrrOC8WLgVC4KF+0q/OpXXtc0P7TbcHdSUp324FHa5XpU6HdX3cly0eN9vhIFC22G//joU2AK7KX1yQZQhzqviw4DhsFp8FQuCictS6eNTGdNLotuDs20kTRPazlSsnlx3yrT8nJ8n5T+BObBkbRYvuTReu6lO1gII+nFIflx6GcbgAULoZB18XT6Lbgx2S5u1KvOl++oo9jvtKsPT4ORYvtc4vUQI5jyYsk5ieITBxGcUj/FC4GQNfFkyank0a3BXdLk0VXVobhoODFOl3hi0PRYvuTxdRtdGBPDELXAVfgOgPtNqc80ZX64bBYZ+F+sgsG4zjYST5NGI5zUSUi3Rb8mKR061oMqSJ/nidd/+S/X+S/37aXk8v/l/88zq8heN8e1/PAY9FdyfLRwOIwxd3sWhzO8quEOFS02H78p2M+pG6jRbynGPpfjqmLeyb4i+/9L/nvk4Hsg72cd70tKA4XtyoNxSLuFnG4yBFqisPF2PKbkVbhgh6lpLxNzk+CFS/SSeow4OR00ui24O64KPnKymVOQv7OMbXKROv8jv2xSF7Sfvm14In0Tk5UpoUky4sEfxhxuNqE//yeyUNfcaho0Y/Su94ubn0HLlf4f0/vGRPS61XTLRhd6r55036Wv9r9cS4Oi4/DSVPuxY391NHV7o9TQ22B8127YFATrzSIfA22WS+iXels99NZsMLFNFLhot0/Rz1OnHrZF3mS/rnAE/FlTjD+avfb6Rb2UUqaXxdaxHjb7qMPBRQuPhe4f7/H4YaTwVzISHH4+4bH8d6LFu1nnff01mmSM+3pM6ficYlr06Q4+TN9D9p9N9vwPtrL43CJRYy0b16uOInuIw7Tvv0kDh+NwzQOHxQYh5dX85PgcciPrHExINa6WGryNWl0W/CjN4UVLdJ3/bDpCoOHmy5a5PHlsn2dtK+X6X3zWFPSSf9d9EWDC7xF5HscjkaH27iClRLN9nWSJ9YpDj9sIA51WvQT/ztNed1GJ3kinl4fNj1ZzN+Bi/x9+0/+/pUUp+PoxzjH4UdxuFQcvr0WhyXlsiUeYxQuBskTRh6ZvDhe3EhSuqeIlJIsz5quK+VFLiL0UjhIRdJUMMkTx+NCChgpUQl7JTff1/+6pIJFm7C+yEWEfuKwTc5T4rzmOFS06M/7ppwrt6lgtijY9RYr+fuXisnTgiaOb4I/3eFdQXF4EigOp4XF4X5efBWFC3obPHRdPDRBTQNUpEFKt0WcZDm6y6uJYlewCBMzuQvjqP3jy4Djzt2JSjcOmLQ9PQ7fLgoWYeKw68JIcfjimXGoaNHX+bmbQBwUsKmneaL4dhtXtVf4Dixu70kF5VkB+/F90DhMBZUS1ro6v1awiBiHvxUSh7ouCqNwMUzRruLv5/vj+6bbgptJSrxi1l26K3tfpmELA9c6MKYFJCvhEpU8adsvZMIWdp2QXMB4ahwqWjg/P2SW4+O3SBPFO74D6TzxMp83Ipu0495BwO2KfiEjjVMpBqfB4/A0x2H0PHccNA65h8LFAAXsukhFi14r2Pl2lUgTVN0WJrHLJShfpm/7uiXkCWPPeRO/+2LcjgfREpV34eOwm7CVEYfdUwtWmbwpWvR5fu4mDpPAm3g1CSvlaRi5gJduoZo2sW/jexcsDicFxOGLUp6Gca0TLvoFjfd5XRMKoHAxXNGqnK977rrQbcHNJKWbvI6Dbt5F03VZFPe4rnz7SLrqfRg4aQ4zHgRPli/yhK28OPw+efvtkThUtPB9fMhhSUW7W9+B86a7feo86CZGu9odOQ7fFh6HqZAc9TzS+8VVlqdwMVC6Lm5MUNPkNNLJUbeFJOUhV0/uKKXL4oExKI0/Ua/4Req6iNqafJIn9LOi47Arutx3xU/Romd54joOuGkpNl5GWsvlifF/mdcciPo53gWJw0kTs4C8GKM+DCAOUxE56kW717ouyqBwMWy6LmJOUHVb9J2kxO22eJu7FQah/SxXnSNNzMf19T4u5GQ54ur6J3nRt0E84z4XJl7eikNFC9/D+yw6jQYTG3ntl4jnlihdFxHjcDFGnQ8oDo+CxqGui0IoXAyYrgvdFtwr4mMn01NDPgxtR+fOkWkTr3gRoeviXcg47CY5w4rDrgiziENFiwDaCWtakHYcbLMumgF0Gt3zHTgJOml813McTpp43Rbd7aIDHKMCx+HvRuX4FC6Gr/auC90W3ExSuieJRLvKfRj5qSHPTlTiFi96S1TyY/cm4eKw8Nb4RxLmRRy+VLSIkQ8EnCxOh9JpVNCkcZyLWOJQHPYdhweG5dgULgau5q4L3RZEm6zeO1kccNHi2lgUsXgxaceJvopY0ZLl4yEXLa4lzJdDvJpemnaCkM7PE5NFk8Y+z8s5DiM9inrxFCdxKD/kDgoXdai160K3BTeTlC7uDgJt0kkNRYtvicr34kWkpGzrBYS8CNh+qDjs7j2GwX7vHpksHtYwWbw1aYyUk+znIsK2HQSLw2lNhdUch28DbdKkpzhkSQoXdUwW0iAYaXK08a4L3RYUkKScDmkhzhXGo0XxIkzC3NN7RlnB/HyIa1pgLF5Bleud5GLlSeUxEekK+2GlcfghWBy+bghL4aIetXVd6LYgcpIya2IuTrWdRKV72kiUz7/TwyKdUeLwqi3ZsMA25fUMohTu3la+3km62h3l8/++5ThMtwmOo+SI+dHNdeYEXfE8ShzuN4SlcFHPRCFNlE4CbdLGWqV1W/BAXERZlPO33HlQ85iUxqMoidqvW0yWUxxOwsRhRe3xhPFrkO04zVd76x2Hu+9/mjRGGAfGuZiwLVGurJ+7Va87HwWKQ8WLoBQu6hLtqv+7wn7vUPZ7raKciI5zxwFxEub9LT7tKE4cjkbnQpBKx+LFhL16ueMkSp6yza6LiTgMFYezQHH4yhGJSeGipkEhXtfFeN0t2rotCJIQ3WfWxsORQ/FtTIqUtG0riY2QEKVzwQcRyLYFuk3kULfRjUljGg8i5CqTLcVhlNtEjj3lKGQc6rgI6ie7oDrHwSb2qTviZM2/L9r+pu9kubuavhdkW84ckZBS+/o2bl2JkBCZtNGXCIW785rXE3hAWu/ic8/bsJdup9vCZD7KxPTX9vP+KvRuGEfYhlTcqnz9m5AULiqTui7aiVMqFBxEGaBS18U6Hgmp24ICkpRxE2cxMG6abPoNgtw3e+4WESofi7Xm35UftpO0doyKkB+msfhkw+8RpVgwEXmhcwKFi2DcKlKnoa51oduC+7hfkceMc/Fz6HFo0kYv8sK0454340Rrfvi85dcNx2GYDkzkjaxG4aJCQ1zrQrcFj5jYBQSIk77j0KSN2sdhFxQeyg+78aHv/HCv8N+P8YoNUbio19C6LnRbcKe8vsXYnmAJm77C0nfC/KdDTI9+6fn9Fe7KyF/GuTvHhJQ+7Wz58bwsQeGiUkPqutBtQfDJImIltSf3nSxfWNuCysdihbtl8sOuuHM64FhxCwDyx0IpXNRtKF0Xui14yMQuIECS0ncC9IfDS8Vj8UzhbiV9F3mGPBZTjl/sglgULio2hK4L3RY48bBO7ZgyGWgcevwj/X2v+m+5Fv+r5Ifd42L7fGTyqw3FYbp1dMcRZkmKXMEoXBCtO+D1iv+9bgseM7YLCBAvfcbhaTsRuXRoqXgcdpvIE8aNAcaLiSirmNgFsShcVC5g18Xeslc8dVsgUUHCvJS/HVYqHofTbSIXDsHK/jIOU7vcpUMQChckpa51oduCh084XXELVrGpWzr6TH7OHVZ69rP4L06v+21DtxeZhLIqxa5AFC6I2HUxeazrQrcFSxrbBfSd2Pb8RJFLV5upfCzWcfSU3LC7vazPsWMTRQZPFCFCHPJEChcslNZ1odsCJxw2YWhXVxQtqH0s9h0oc9+50o2cgBsULrhSUteFbguccChsgmV9C4zFfeU3Oo6e45+BjcVjhxTKpXDBdaV0Xei2AErS59Xmmd1PxRQt7L/rxg4pK/rFLohD4YJvSui6aP+eJgD7gbZRtwUMTDvODKlTZ+aIUjGPAS53/5kwEoFbjgNRuOC26F0Xb4INIrotYrMQF7UnKiZu9KrnxWl1XDxDz7fZmDACNyhccPMkFbjrIndbvA60bbotgGX0duXQ/f1U7n92AVc55GYerwpskcIFd4nadaHbAiiRK4dAqYbStWUchsIpXPCDwF0Xui0AALZH1xYQgsIF94nWTfCp0W0BAABQHYUL7hSw6yJS0UK3BQAAwJYoXPAQXQX2CwAAQK8ULrhXwK6LCHRbAAAAbJHCBY/RXWB/AM/T2+J28/ncSvpA9Uaj0bm9AGVTuODhgV7XxXW6Lcozswt4wri37u/5/3r8OHuOKAA8yaVdEIfCBcvQZWA/lOpfu4DK6bgAgKf5xy6IQ+GCR+m6uKLbAuowG9jn0XEBABRP4YJlHfv8mIQiZp7kvMfP84tDCtD7WIyY4ZkULlhK5V0Xui1MQqnHxcA+j44LAHgaa1wEonDBKo59bpxwGLhNrIvSZzFk7MkiAL2PxRRoNBqJmUB+sgtY+sv7ZTqb756dtH88qOhj67YoO2Yv2pjtcxMO2204cSSqT3wu5/N5n5uw31inCKDPJzxdtueC/zgE8HQ6LljVsc9LYWY9vvcru5/sQhwC9Oq8x/femc/nbt2DZ1C4YCWVrXWh22IYZj2+977djzgECKHv20d/dwjg6RQueIpjn5OC/N3je+/Md89MGkn+6TUO53NxCFQtwHoFxmF4BoULVh/46+i60G0xHLOe3/9Xh4Cm/0eqiUOAfsfitFjyxCGAp1G44KmOfT4kKUs5mO+eeaoDF73H4Xw+dhgAY3Gv3C4CT6RwwZMMvOtCt8XwYrXv+1rfOBKVx+FolGJw1vNmHDgSQOX+6fn9FZHhiRQueI5jn4tCnPf8/q91XRAiDudzcQgYh/t14DDA6hQueLKBdl3othimv3t+/zRZ1HWBOAToM3cdjVLuOut5M17ruoDVKVzwXMc+DwU4D7ANqetCoiIO+49DCTNgLO5TKiK/cxhgNQoXPMvAui50Www3TtNiXLMAicp7R6PiOOyu9F2IQ4Be/RVgGw48YQRWo3DBOhz7HBTgNMA27M93zyQqdTsPEYfz+b5DARiHe6WIDCtQuODZBtJ1odti+P4Osh0fLdRZtT/DxKGFOoEa89buKU8RLmbstePwkSMCy1G4YF2ObT+hE5Uv05SkXAbYlHGaNDoi1SbMEW5bSlLR4pMjAlTqryDb8c4tI7AchQvWNSlMifhJoZuv26IeUWI03TLi6Q71itJ1MXG1D6hUlIsZyScdcPA4hQvW6dh2Y8K4tPfz3TPrDNTpJNC2pKt9Bw4JUJNAt4skqWhxpngBD1O4YH0ngTK7LnRb1BWjqU3/ItAmpfUu9hyZ6hLmNFZGGnfetwmzOARqE+liRhqD3UYKD1C4YN2ObS/B/RFoW7qrLIoXEuYIcah4AVRkNBqdN7EuZqQnPilewD0ULljvSaCsrgvdFnXGaIrPy0CbVEXxov18qbtEQvY9YU5xOAsXhwMvXqTbYkwMgGv+CLY9gx+j0i0x7UuxnJUpXLAJx7YTicoTJo0DLV7kgsXBVUKmeBE/DgeaTLaf633TtWIrXgBXchH5MthmDXaMyueXz+1r0uj0Y0UKF6z/JFBG14Vui7p9CJioLIoXg1mws/0sO9eKFt8TMsWLhZOgcfh5aAt25knAmxomBsDK/gi4TWmM+jykBTtzkeKs6R4L/z3vUbxgSQoXbMqx7SOq0ZfpZdBEJZ3EPw3hUantZxjnBOXgzoRM8WKxqv0fQTfv4xAelZpbkj/fG4eKF0DMixnJ3lAm9rkY/jnnObfzHsULlqJwwaYmhrMmbteFbgsiJypJelRqKmAUeaUld418zknXfRQv4sfhu3wfcplxOJ9P2h9fH41DxQuoO2eNXUReFC8OCh2Hd/IY+9A4q3jBUhQu2KRj20XYRCVu18VCmvx/bSf3k2ISlO7WkLSOwKfmx6sqd08aKy9e5IQ58ph0NflvE8pibmHKiXKKw7Ol41DxAurOCUajoybWgsm3J/apC+5TSYXkXDy+r+Ptrs+oeMGDFC7Y5MQwnQBOgm2Wbguu+xA4Ufl+Iu+6L8ahE5TvXRar3uaieDEalRCHn3LSHDsOvyfKq8eh4gXULvqFre6CRvDui1vF41XOGYoXPEjhgtpOArot+D5h7Lou3hawqVdFgXaCfxTt9pHUEdK+UnLyacUE5eak0W0jh4XEYUqaj6Jd9UuJbrqt5QmJ8s04VLyAenOC7gkj58E3c9F98TkXakPJayOlW/TePOPzKV5wJ4ULNj0xnDVxui50W3BXjJ4WkKgsTubvmu72kd4LGNcKFum1juSp6uJFmzCnGDwtZHO7OOwKGONe47BN3FMnSPP98XrPj0PFC6jZ20K2c7H2xVnft/LlDot0Pviazw/PzU8UL7iTwgXbcGw7CC5d7b4sZFsXBYz/pon+Nh+fmm5XSU88aV8pOVlXweLmpLHuzou3Bcbh13wLyfbisEuS3+SnhaQ43F97HCpeQJVGo9FF091GWop0Hk5j8NaLybnTLY2Vi4LFOt9b8YIfKFyw+ZNAjK4L3RY8FqN/FLjpB033+NRvRYx1d2K0v28vFys+5+Tk/ZqTkx8njZUWL9qEOcVhiQXW/Zw4/zclsen+63UnzzlBfpNvB/lvjsO9jcah4gXUKo3Ds8K2edx8LyZ/zkWMyZrH4VQ03k/rV+TuisXCm5vqAFW84Iaf7AK2eBI46Pn94f5J45dpuv3iVbP+LoJt2Mnfr4NcbEgJV7pq9E/+ma7iz3KB5t4CRf496efP+efeBhOSx4oX6ZgcVheHo9GHNkn7dRBxOJ/fHYddgebeAkX+PeP8etVrHM7n6ZhUF4dQdT4wGl223/30vT8r9CMszt/pkdbp7+d5DP43/1zcnnhvgaL5Xhie3MoJ+jivpOLFNHfDoHABG58UztqJyElPxQvdFiwrJSqfe5okrdNi0rd/qzhR0meotnjR+q3puluGGYddIl1OHCpeQH15azuxb7/76aLXuwF8nElzqxhe2DiseMEVt4qwTceVvS+lJSpdR8JbeyLQpLHC20bS1b6mjKeM1BOHbhuB+nKC0eioyR0K9M5tIyhcsPVJ4cmW31a3BavG6UkT50k41Fu8SE8Y+eDwB4pDxQuoUeqAu7QbQlC8qJzCBdt2PPD3YwiTxu72BFdZIk0a6yxepO6fc4c/UBwqXkBt4/Cs6YoXxKB4UTGFC7Y9IUwngJMtvZ1uC55j2rjKEmvSWOfTRlLCrIgWKQ4VL6Cu3LVbyNLte3EoXlRK4YI+HA/sfRhiovJlmooWihfRJo2VFS+urXchDiPFoeIF1JUTjEYnjdtII1G8qJDCBX1MCGdbGPx1W7COWL1oXGWJN2msr3iR4nDq0AeLQ8ULqCsn6J4udGpPhKF4URmFC/pyXPjvp5ZE5cs0JSmKF9EmjXUWL8RhtDhUvIDaWAMrFsWLiihc0NdkcNZsrutCtwXrjtcTk8aAk8b6ihfiMGIcKl5ATePw4jZSxYs4FC8qoXBBn44L+73UnKwoXsScNO6eva8saRaHEeNwPn9vN0A147DiRTyL4sXYrhguhQv6nAjOmvV3Xei2YJMxa9IYS0oe/6wwaRaH4hDodxxWvIjnND++loFSuKBvx8F/H9xMVhQvokjJ4ou8gGqNSbM4jOFq8pLXIAHqGocVL+I4yYunMmAKF/Q9CZw16+u60G3BtuLWpLFfV0/ZyI+srTlpFof9x+ELRQuoehxWvOjfW0WLOihcEMFxsN8DjycrXfHit6a74sr2nDeKFteT5hSHL8Xh1nXFs27SAtQ9Dl+2rzQOn9gbW3fY7vsPdkMdFC6IMAGcrWGw121BH7GbHpU6NWncmpN2nyta/Jg0XzSu+G03DttJiqIFcGssTlf9XUTbjsVteid2RT0ULojiuOf/H56WqHRrLLwwady4w3ZfawW9P2FeFC/O7Y2N0pIMPDQWHzXdLXwKm5uz6HhzvquMwgVRJn+z5uldF7ot6Dt+L9tXahPVrrh+Kfl7mW/N4eGEObUrT8XhxuJwqiUZWGIsTucrXXCbcdJYELlaChdEcrzl/w/Wm6x8mb5trHuxTudNxU8OeUbSLA7XH4cvXd0DVhiHF11wp/bGWqTz2VXHm9v06qVwQaRJ36xZvetCtwXR4jglKS8bLfvP9dZ6Fs9KmlMcvhCHz3aculja18yuAFYch1MXXCoiu3XkeRa3huh4q5zCBeGSxA3/97D5ZOXLdJYm3WnyLVlZWZpop1tDJCjrSZrF4dMT5Zf5fnWA54zFJ40LGk+eF+TFkHVeonBBvAlfs3zXhW4LosfzB8nK0ro20K7LQoKy3qR5EYdalpeLQ4kysO5xeJYLybovlszxG8VjblG4IKLjNf930F+y8r37IrWLzuyRO5003VoWuiw2mzSnGJyKw3udSpSBDY/FV+e7xiLK90lFncN8i57iMTcoXBByotc83nWh24LS4vq0faVk5bhxteXb9zhNpNNjTq1lsbWk+bx9pTh0+8jtOByNfrOWBbCFcfgyL6KcxmKdcJ3LnB+9yMUd+IHCBVEdP/PfQ8yE5cv0KCcrNRcwuolid1vIuajoJXH+IA6/FSymnhgC9DAOX++Eq3UMul6wOPLEEB6icEHUyd2sub/rQrcFpcf35a0CxqyqiaKCRZSk+TLfFrHowKgrDhUsgBhj8Xle/2LarP50vVIpWLAyhQsiO17xn0NZyUouYORbSA6bYV5xScnI1dV9BYuwSXMqYHzIt5D8NuA4PMlJsoIFEHEsTgWMlAsMuRsurVuR1rD4j4IFq1K4IPKkbtb8WHnWbcFQ4/0kL+K5WLRrVvhHOr1KTr5M/9O+3ubvM/ET59N85W8o3UBXcdh0BYtDa1gABYzDszyp/0/TFZNLXwdjlvOal/mJTSeOMk/xk11AcClxPrj1dxhuwtJN8FPb/tv57tle+/P39jVpX3uFTBL/Sj8ttll+4tz+OEqv+Xxebhy6mgeUPRan8ey0HYd32p/77evXPBbvBN/0WR6L//R0ENZF4YLwk7h28nbSdMUL3RbUFv/pZH91wm+/B+OcrLzKP8cBNjF9H//23Rx84vw9DufzqHGYtu/vnOQDDG0cXtzudpLH4smtsbhvs8U43HRF45mjxropXFCCRdeFbgvqTVq+3zrVJS27Z+lqy15OWH7JE8hNXQ2/zAlJev2bfipUVJs8z24lz7fjcGeDSfSPcWitCqDOsTiNfd/Gv9wZt5fH4b2cE4w39PZpDE7ngn8WY7JCBdugcEERE7Z2knZoorQ1s6a/xfm0dS//vbjMx+nGsbpW0GjumEC+euCY/3vr7+l1mbs+4L7k+e44/F7QWCUOL3MifDsOm0oLFH195nV/5y97/CwmU/HiIfr7ljoWX9y1z3JBI43H4+ZmIePn5v7CRvo9/7tjLJopUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADD9/8FGADBapp4gTV6PAAAAABJRU5ErkJggg=="
                  />
                </defs>
              </svg>
            </NavLink>
            <div style={{
              flexGrow: 1
            }}><SearchBar /></div>
            <button
            onClick={showDrawer}
              type="button"
              class="inline-flex items-center p-2  text-sm text-gray-500 rounded-lg xl:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
              aria-controls="navbar-default"
              aria-expanded="false"
            >
              <span class="sr-only">Open main menu</span>
              <svg
                class="w-8 h-8"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </button>
            
          </div>
          <div class="relative hidden w-full lg:block lg:w-auto" id="navbar-default">
          <Drawer
              placement="top"
              closable={false}
              onClose={onClose}
              open={open}
              className='p=0 absolute top-16'
              height={210}
              
            >
              <ul class="flex flex-col p-4  border  space-y-2   bg-gray-800  border-gray-700 divide-y divide-gray-600">
                <li class="block text-right py-2 pl-3 pr-4 text-white hover:bg-purple-900  md:hover:text-pink-600   hover:text-white md:hover:bg-transparent">
                  <Link
                    to="movie-list"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    {" "}
                    Movie
                  </Link>
                </li>
                <li class="block text-right py-2 pl-3 pr-4 text-white hover:bg-purple-900  md:hover:text-pink-600   hover:text-white md:hover:bg-transparent">
                  <Link
                    to="cine-system"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    Cinema
                  </Link>
                </li>
                <li class="block text-right py-2 pl-3 pr-4 text-white hover:bg-purple-900  md:hover:text-pink-600   hover:text-white md:hover:bg-transparent">
                  <Link
                    to="footer"
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    Offer
                  </Link>
                </li>
                <li className='py-2'>
                  <span
                    className=" text-white "
                  >
                    <UserMenuTablet />{" "}
                  </span>
                </li>
              </ul>
            </Drawer>
            </div>
        </nav>
      </div>
    </div>
  );
}
