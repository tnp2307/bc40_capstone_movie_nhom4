import React from "react";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Space } from "antd";
import { useSelector } from "react-redux";
import { localStore } from "../../service/localUserStorage";
import { NavLink } from "react-router-dom";

const UserMenuMobile = () => {
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });

  let handleLogout = () => {
    localStore.remove();
    window.location.reload();
    // window.location.href = "/login";
  };
  let renderMenu = () => {
    let buttonCss = "px-6 py-2 rounded-xl hover:text-green-200";
    if (userInfor) {
      return (
        <div className="flex flex-col justify-start items-end gap-5">
          <Dropdown
            menu={{
              items: [
                { label: <span>Detail </span>, key: "0" },
                { label: <button onClick={handleLogout}>Logout</button> },
              ],
            }}
            trigger={["click"]}
          >
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                {userInfor.hoTen}
                <DownOutlined />
              </Space>
            </a>
          </Dropdown>
        </div>
      );
    } else {
      return (
        <>
          <div className="flex justify-start items-end flex-col gap-5">
            <NavLink to={"/login"}>
              <button
                className={buttonCss}
                style={{
                  background:
                    "linear-gradient(154deg, #9580FF 12.50%, rgba(149, 128, 255, 0.39) 100%)",
                  border:"0.25px solid gray"
                }}
              >
                Login
              </button>
            </NavLink>
          </div>
        </>
      );
    }
  };
  return renderMenu();
};

export default UserMenuMobile;
