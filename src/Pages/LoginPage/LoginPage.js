import React from "react";
import { Button, Form, Input, Tooltip, Typography, message } from "antd";
import { userService } from "../../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { localStore } from "../../service/localUserStorage";
import { setLoginAction } from "../../reduxToolKit/userSlice";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postLogin(values)
      .then((res) => {
        dispatch(setLoginAction(res.data.content));
        localStore.set(res.data.content);
        message.success("Login successful");
        navigate("/homepage");
      })
      .catch((err) => {
        console.log(err);
        message.error("Login fail");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="relative min-h-screen bg-purple-100 backdrop-blur flex justify-center items-center bg-texture bg-cover py-28 sm:py-0">
      <div className="p-4 flex-1">
        <div className="max-w-[640px] min-w-[420px] bg-white rounded-3xl p-4 mx-auto">
          <div class="mx-auto text-center">
            <img className="mx-auto" src="./resource/loginImg.png" alt="" />
            <h1 class="text-4xl text-gray-800">Log In</h1>
          </div>
          <Form
            name="basic"
            labelCol={{
              span: 12,
            }}
            wrapperCol={{
              span: 24,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input className="peer w-full px-0.5 border-0 border-b-2 border-gray-300 placeholder-transparent focus:ring-0 focus:border-purple-600" />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password className="peer w-full px-0.5 border-0 border-b-2 border-gray-300 placeholder-transparent focus:ring-0 focus:border-purple-600" />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                span: 24,
              }}
            >
              <Button
                className="w-full my-8 py-8 text-lg text-white font-semibold text-center rounded-full bg-purple-500 transition-all hover:bg-purple-600 focus:outline-none inline-flex items-center justify-center"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>

            <Form.Item>
              <Tooltip title="Sign Up!">
                <Typography.Link href="/sign-up">Don't have an account?</Typography.Link>
              </Tooltip>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;
