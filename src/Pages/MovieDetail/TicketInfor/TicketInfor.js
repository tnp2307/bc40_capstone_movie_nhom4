import React, { memo, useCallback, useEffect, useState } from "react";
import { movieSer } from "../../../service/movieService";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import {
  setLoadingOff,
  setLoadingOn,
} from "../../../reduxToolKit/spinnerSlice";
import { setSelectMovieAction } from "../../../reduxToolKit/selectMovieSlice";
import { message } from "antd";
import { useNavigate } from "react-router-dom";

function TicketInfor(props) {
  const selectedSeatList = useSelector((state) => {
    return state.selectMovieSlice.selectedSeatList;
  });
  const [seatList, setSeatList] = useState([]);
  const [movieInfor, setMovieInfor] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    if (props.data!=null) {
       movieSer
      .getTheaterList(props.data)
      .then((res) => {
        setMovieInfor(res.data.content.thongTinPhim);
        let newList = [];
        res.data.content.danhSachGhe.map((item) => {
          return newList.push({ ...item });
        });

        setSeatList(newList);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        console.log(err);
        dispatch(setLoadingOff());
      });
    }
   
  }, [props]);
  let totalPrice = () => {
    let sum = 0;
    selectedSeatList.forEach((element) => {
      sum += element.giaVe;
      return sum;
    });
    return sum;
  };
  let renderTicketInfor = useCallback((selectedSeat) => {
    return (
      <>
        <h2 className=" text-4xl py-2">Ticket Information</h2>
        <hr className="  border-t-4 border-dotted " />
        <h3 className=" text-2xl py-2">Rạp: {movieInfor.tenCumRap}</h3>
        <h3 className=" text-2xl py-2">Địa chỉ: {movieInfor.diaChi}</h3>
        <h3 className=" text-2xl py-2">
          Thời gian: {movieInfor.ngayChieu} - {movieInfor.gioChieu}
        </h3>
        <hr className="  border-t-4 border-dotted " />
        <h3 className=" text-2xl py-2">Phim: {movieInfor.tenPhim}</h3>
        <h3 className=" text-2xl py-2">
          Vé đã chọn:{" "}
          {_.sortBy(selectedSeat, ["tenGhe"]).map((item) => {
            return item.tenGhe + " ,";
          })}
        </h3>

        <hr className=" border-t-4 border-dotted " />

        <h3 className=" text-2xl py-2">Tổng cộng:{totalPrice()}</h3>
        <button
          onClick={() => {
            bookTickets();
          }}
          className="bg-[#9580ff] px-2 py-3 rounded-full"
        >
          Đặt vé
        </button>
      </>
    );
  });
  class TicketInfor {
    maLichChieu = 0;
    danhSachVe = [];
    constructor() {}
  }
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  }); 
  let navigate = useNavigate()
  const bookTickets = () => {
    dispatch(setLoadingOn());
    
    if (userInfor==null){
      navigate("/login")
      dispatch(setLoadingOff());
    }
    else{
      let bookingTicketInfor = new TicketInfor();
    bookingTicketInfor.maLichChieu = props.data;
    bookingTicketInfor.danhSachVe = [...selectedSeatList];
    movieSer
      .postBookTicket(bookingTicketInfor)
      .then((res) => {
        movieSer
        .getTheaterList(props.data)
        .then((res) => {
          setMovieInfor(res.data.content.thongTinPhim);
          let newList = [];
          res.data.content.danhSachGhe.map((item) => {
            newList.push({ ...item });
          });
          setSeatList(newList);
          message.success(`Your ticket was booked successfully`);
          dispatch(setLoadingOff());
        })
        
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
        message.error(`Your ticket was failed to be booked : ${err.response.statusText}`);
      });
    }
    
  };

  let selectSeat = (item) => {
    dispatch(setSelectMovieAction(item));
  };
  return (
    <>
      <div className="w-full p-4 col-span-2 bg-[#2f274e] border rounded-lg">
        <div className=" m-8 py-4 text-lg text-white font-semibold text-center border-0 border-b-2 border-gray-300 rounded">
          <div className="flex flex-row justify-evenly w-auto pb-4">
            <div>
              <button className="py-2 px-4 rounded-md text-center bg-gray-400">
                01
              </button>
              <h2>Booked seat</h2>
            </div>
            <div>
              <button className="py-2 px-4 rounded-md text-center bg-red-400">
                01
              </button>
              <h2>VIP seat</h2>
            </div>
            <div>
              <button className="py-2 px-4 rounded-md text-center bg-purple-400">
                01
              </button>
              <h2>Normal seat</h2>
            </div>
            <div>
              <button className="py-2 px-4 rounded-md text-center bg-green-400">
                01
              </button>
              <h2>Choosing seat seat</h2>
            </div>
          </div>
          <h2 id="screen">Screen</h2>
          <br />
        </div>
        <div className="grid grid-cols-10 gap-4">
          {seatList.map((item) => {
            if (item.daDat === false) {
              let index = selectedSeatList.findIndex(
                (seat) => seat.maGhe == item.maGhe
              );
              if (index != -1) {
                return (
                  <button
                    onClick={() => selectSeat(item)}
                    className="py-2 px-4 rounded-md text-center bg-green-400"
                  >
                    {item.tenGhe}
                  </button>
                );
              } else if (item.loaiGhe != "Thuong") {
                return (
                  <button
                    onClick={() => selectSeat(item)}
                    className="py-2 px-4 rounded-md text-center bg-red-400"
                  >
                    {item.tenGhe}
                  </button>
                );
              } else {
                return (
                  <button
                    onClick={() => selectSeat(item)}
                    className="py-2 px-4 rounded-md text-center bg-purple-400"
                  >
                    {item.tenGhe}
                  </button>
                );
              }
            } else {
              return (
                <button
                  disabled={item.daDat}
                  className="py-2 px-4 rounded-md bg-gray-500"
                >
                  x
                </button>
              );
            }
          })}
        </div>
      </div>

      <div className="w-full p-12 bg-white border rounded-lg">
        {renderTicketInfor(selectedSeatList)}
      </div>
    </>
  );
}
export default memo(TicketInfor);
