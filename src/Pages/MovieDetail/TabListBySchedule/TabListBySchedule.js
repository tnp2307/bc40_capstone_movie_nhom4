import React, { useEffect, useState } from "react";
import { movieSer } from "../../../service/movieService";
import { Tabs } from "antd";
import ItemTabLichChieu from "./ItemTabLichChieu";

export default function TabListBySchedule(props) {
  const [heThongRap, setHeThongRap] = useState([]);
  useEffect(() => {
    movieSer
      .getScheduleByMovie(props.id)
      .then((res) => {
        setHeThongRap(res.data.content.heThongRapChieu);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderSchedule = () => {
    return heThongRap.map((item) => {
      return {
        key: item.maHeThongRap,
        label: (
          <div className="bg-white flex items-center justify-center cine-system">
            <img className="h-16 w-16" src={item.logo} />
          </div>
        ),
        children: (
          <Tabs
            id="cine-list"
            style={{ height: 300 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={item.cumRapChieu.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="text-white cine-name flex flex-col justify-evenly items-start py-5">
                    {cumRap.tenCumRap}
                  </div>
                ),
                children: (
                  <div
                    style={{ height: 300 }}
                    id="schedule-infor"
                    className=" content overflow-y-scroll"
                  >
                    {cumRap.lichChieuPhim.slice(0, 20).map((phim) => {
                      return (
                        <ItemTabLichChieu phim={phim} key={phim.maLichChieu} />
                      );
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <>
      {" "}
      <Tabs
        id="cine-system"
        className="container "
        style={{ height: 500 }}
        tabPosition="top"
        defaultActiveKey="1"
        items={renderSchedule()}
      />
    </>
  );
}
