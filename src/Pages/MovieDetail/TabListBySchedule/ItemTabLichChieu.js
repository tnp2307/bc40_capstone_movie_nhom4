import moment from "moment";
import React from "react";
import { movieSer } from "../../../service/movieService";
import { useDispatch } from "react-redux";
import { setSelectMovieAction } from "../../../reduxToolKit/userSlice";
import { setLoadingOff, setLoadingOn } from "../../../reduxToolKit/spinnerSlice";

export default function ItemTabLichChieu(props) {
  let dispatch = useDispatch();
  let getTheaterSeats = (id) => {
    dispatch(setLoadingOn());
    movieSer
      .getTheaterList(id)
      .then((res) => {
        dispatch(setSelectMovieAction(id));
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        console.log(err);
        dispatch(setLoadingOff());
      });
      
  };
  return (
    <button
      onClick={() => {
        getTheaterSeats(props.phim.maLichChieu);
      }}
      className="m-4 rounded px-4 py-2 bg-purple-300 text-white"
    >
      <h2>{props.phim.tenRap}</h2>
      {moment(props.phim.tenRap).format("DD/MM/YYYY")}
      <br />
      {moment(props.phim.tenRap).format("hh:mm")}
    </button>
  );
}
