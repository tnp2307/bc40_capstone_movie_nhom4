import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Logo from "../../resource/Rectangle16.png";
import TabListBySchedule from "./TabListBySchedule/TabListBySchedule";
import { movieSer } from "../../service/movieService";
import { setLoadingOff, setLoadingOn } from "../../reduxToolKit/spinnerSlice";
import { useDispatch, useSelector } from "react-redux";
import TicketInfor from "./TicketInfor/TicketInfor";

export default function MovieDetail() {
  const dispatch = useDispatch();
  let { id } = useParams();
  let selectedMovie = useSelector((state) => {
    return state.userSlice.selectedMovie;
  });
  const [detail, setDetail] = useState({});

  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(setLoadingOn());
    movieSer
      .getMovieDetail(id)
      .then((res) => {
        setDetail(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
      });
  }, [id]);

  return (
    <div className=" relative">
      <div id="detail-movie" className="container flex py-16">
        <img src={detail.hinhAnh} id="movie-img" />
        <div className=" ml-12 text-white">
          <h2 className=" text-5xl font-bold py-4">{detail.tenPhim}</h2>
          <div className="movie-infor flex">
            <div className="flex mr-6">
              <div className="mr-2">
                <img width={40} height={20} src={Logo} alt="" />
              </div>
              <h3>{detail.danhGia}/10</h3>
            </div>
            <div className="flex items-center mr-6">
              <svg
                width="15"
                height="15"
                viewBox="0 0 15 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M7.29004 14.8311C11.1045 14.8311 14.2627 11.666 14.2627 7.8584C14.2627 4.04395 11.0977 0.885742 7.2832 0.885742C3.47559 0.885742 0.317383 4.04395 0.317383 7.8584C0.317383 11.666 3.48242 14.8311 7.29004 14.8311ZM3.71484 8.59668C3.44141 8.59668 3.23633 8.3916 3.23633 8.11816C3.23633 7.85156 3.44141 7.64648 3.71484 7.64648H6.81152V3.51074C6.81152 3.24414 7.0166 3.03906 7.2832 3.03906C7.5498 3.03906 7.76172 3.24414 7.76172 3.51074V8.11816C7.76172 8.3916 7.5498 8.59668 7.2832 8.59668H3.71484Z"
                  fill="white"
                />
              </svg>
              <h3 className="ml-2">2h 10 min</h3>
            </div>
            <h3 className="mr-6">2023. Thrilling / Mystery</h3>
          </div>
          <h2 className=" text-xl font-semi-bold py-4">{detail.moTa}</h2>

          <a
            className="rounded px-5 py-2 bg-red-500 text-white font-medium "
            href="#cine-system"
          >
            Mua Vé
          </a>
        </div>
      </div>

      <div className="container py-4 grid grid-cols-1 gap-4 bg-[#2f274e] border-none rounded-lg">
        <TabListBySchedule id={id} />
      </div>

      <div className="container py-4 grid grid-cols-3 gap-4 justify-between border-none">
        <TicketInfor data={selectedMovie} />
      </div>
    </div>
  );
}
