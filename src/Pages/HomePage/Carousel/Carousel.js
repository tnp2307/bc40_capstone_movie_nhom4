import { Button, Carousel } from "antd";
import { useEffect, useState, useRef } from "react";
import { movieSer } from "../../../service/movieService";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { NavLink } from "react-router-dom";

const App = () => {
  const [movie, setMovies] = useState([]);
  const carouselRef = useRef();

  useEffect(() => {
    movieSer
      .getMovieBanner()
      .then((res) => {
        setMovies(res.data.content);
        
      })
      .catch((err) => {});
  }, []);

  const next = () => {
    carouselRef.current.next();
  };

  const previous = () => {
    carouselRef.current.prev();
  };
  let renderCarousel = () => {
    return movie.splice(0, 10).map((item) => {
      return (
        <NavLink id="carousel-link" className to={`/detail/${item.maPhim}`}>
          <div
            id="carousel"
            style={{
              backgroundImage: `url(${item.hinhAnh})`,
            }}
          ></div>
        </NavLink>
      );
    });
  };

  return (
    <div id="carousel-main">
      <Carousel autoplay ref={carouselRef} className="pb-10 mb-10">{renderCarousel()}</Carousel>
      <Button onClick={previous} className=" carousel-button carousel-button-left flex justify-center items-center  md:left-32 sm:left-16 lg:left-60 xl:left-96">
        <LeftOutlined />
      </Button>
      <Button onClick={next} className="carousel-button carousel-button-right flex justify-center items-center md:right-32 sm:right-16 lg:right-60 xl:right-96 ">
        <RightOutlined />
      </Button>
    </div>

  );
};

export default App;
