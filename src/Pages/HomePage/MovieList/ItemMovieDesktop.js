import React, { useCallback, useMemo, useState } from "react";
import { Button, Card } from "antd";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setOpenModal } from "../../../reduxToolKit/movieModalSlice";
const { Meta } = Card;
const ItemMovie = (props) => {
  const { tenPhim, maPhim, trailer, danhGia } = props.movie;
  let dispatch = useDispatch();
  let [isLiked, setIsLike] = useState(false);
  const likeIcon = useMemo(() => {
    return (
      <svg
        width="48"
        height="48"
        viewBox="0 0 48 48"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g filter="url(#filter0_b_96_4203)">
          <rect
            width="48"
            height="48"
            rx="12"
            fill="url(#paint0_linear_96_4203)"
          />
          <path
            d="M24 33.2168C24.2051 33.2168 24.498 33.0801 24.7129 32.9531C30.2109 29.4375 33.6973 25.3457 33.6973 21.1855C33.6973 17.7285 31.3242 15.2871 28.2578 15.2871C26.3535 15.2871 24.8887 16.3418 24 17.9531C23.1309 16.3516 21.6465 15.2871 19.7422 15.2871C16.6758 15.2871 14.3027 17.7285 14.3027 21.1855C14.3027 25.3457 17.7891 29.4375 23.2969 32.9531C23.502 33.0801 23.7949 33.2168 24 33.2168Z"
            fill="#D4FF00"
          />
          <rect
            x="0.5"
            y="0.5"
            width="47"
            height="47"
            rx="11.5"
            stroke="url(#paint1_linear_96_4203)"
          />
        </g>
        <defs>
          <filter
            id="filter0_b_96_4203"
            x="-14"
            y="-14"
            width="76"
            height="76"
            filterUnits="userSpaceOnUse"
            color-interpolation-filters="sRGB"
          >
            <feFlood flood-opacity="0" result="BackgroundImageFix" />
            <feGaussianBlur in="BackgroundImageFix" stdDeviation="7" />
            <feComposite
              in2="SourceAlpha"
              operator="in"
              result="effect1_backgroundBlur_96_4203"
            />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="effect1_backgroundBlur_96_4203"
              result="shape"
            />
          </filter>
          <linearGradient
            id="paint0_linear_96_4203"
            x1="2.36286"
            y1="2.6642"
            x2="24"
            y2="48"
            gradientUnits="userSpaceOnUse"
          >
            <stop offset="0.125" stop-color="#A4A4A4" stop-opacity="0.31" />
            <stop offset="1" stop-color="white" stop-opacity="0.06" />
          </linearGradient>
          <linearGradient
            id="paint1_linear_96_4203"
            x1="2.36286"
            y1="2.6642"
            x2="24"
            y2="48"
            gradientUnits="userSpaceOnUse"
          >
            <stop stop-color="white" />
            <stop offset="1" stop-color="white" stop-opacity="0" />
          </linearGradient>
        </defs>
      </svg>
    );
  }, [isLiked]);
  const unlikeIcon = useMemo(
    () => (
      <svg
        width="49"
        height="49"
        viewBox="0 0 49 49"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g filter="url(#filter0_b_173_2428)">
          <rect
            x="0.869141"
            y="0.315186"
            width="48"
            height="48"
            rx="12"
            fill="url(#paint0_linear_173_2428)"
          />
          <path
            d="M18.0811 21.9451C18.0811 24.8572 20.5215 27.7214 24.377 30.1824C24.5205 30.2712 24.7256 30.3669 24.8691 30.3669C25.0127 30.3669 25.2178 30.2712 25.3682 30.1824C29.2168 27.7214 31.6572 24.8572 31.6572 21.9451C31.6572 19.5251 29.9961 17.8162 27.7812 17.8162C26.5166 17.8162 25.4912 18.4177 24.8691 19.3406C24.2607 18.4246 23.2217 17.8162 21.957 17.8162C19.7422 17.8162 18.0811 19.5251 18.0811 21.9451ZM19.1816 21.9451C19.1816 20.1267 20.3574 18.9167 21.9434 18.9167C23.2285 18.9167 23.9668 19.7166 24.4043 20.4001C24.5889 20.6736 24.7051 20.7488 24.8691 20.7488C25.0332 20.7488 25.1357 20.6667 25.334 20.4001C25.8057 19.7302 26.5166 18.9167 27.7949 18.9167C29.3809 18.9167 30.5566 20.1267 30.5566 21.9451C30.5566 24.488 27.8701 27.2292 25.0127 29.1296C24.9443 29.1775 24.8965 29.2117 24.8691 29.2117C24.8418 29.2117 24.7939 29.1775 24.7324 29.1296C21.8682 27.2292 19.1816 24.488 19.1816 21.9451Z"
            fill="#D4FF00"
          />
          <rect
            x="1.36914"
            y="0.815186"
            width="47"
            height="47"
            rx="11.5"
            stroke="url(#paint1_linear_173_2428)"
          />
        </g>
        <defs>
          <filter
            id="filter0_b_173_2428"
            x="-13.1309"
            y="-13.6848"
            width="76"
            height="76"
            filterUnits="userSpaceOnUse"
            color-interpolation-filters="sRGB"
          >
            <feFlood flood-opacity="0" result="BackgroundImageFix" />
            <feGaussianBlur in="BackgroundImageFix" stdDeviation="7" />
            <feComposite
              in2="SourceAlpha"
              operator="in"
              result="effect1_backgroundBlur_173_2428"
            />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="effect1_backgroundBlur_173_2428"
              result="shape"
            />
          </filter>
          <linearGradient
            id="paint0_linear_173_2428"
            x1="3.23201"
            y1="2.97938"
            x2="24.8691"
            y2="48.3152"
            gradientUnits="userSpaceOnUse"
          >
            <stop offset="0.125" stop-color="#A4A4A4" stop-opacity="0.31" />
            <stop offset="1" stop-color="white" stop-opacity="0.06" />
          </linearGradient>
          <linearGradient
            id="paint1_linear_173_2428"
            x1="3.23201"
            y1="2.97938"
            x2="24.8691"
            y2="48.3152"
            gradientUnits="userSpaceOnUse"
          >
            <stop stop-color="white" />
            <stop offset="1" stop-color="white" stop-opacity="0" />
          </linearGradient>
        </defs>
      </svg>
    ),
    [isLiked]
  );

  const handleOpenModal = (trailer) => (event) => {
    event.stopPropagation();
    event.preventDefault();
    dispatch(setOpenModal(trailer));
  };
  let handleLike = useCallback((event) => {
    event.stopPropagation();
    event.preventDefault();

    if (isLiked == false) {
      setIsLike(true);
      console.log("like");
    } else {
      setIsLike(false);
      console.log("unlike");
    }
  });
  return (
    <NavLink
      className=" item-link "
      style={{
        width: 260,
        height: 472,
      }}
      to={`/detail/${maPhim}`}
    >
      {" "}
      <Card
        className="overflow-hidden movie-item  "
        style={{
          width: 260,
          height: 472,
          borderRadius: 24,
        }}
        cover={
          <div
            className="hover:no-underline relative"
            style={{
              backgroundImage: `url(${props.movie.hinhAnh})`,
              backgroundSize: "cover",
              height: 353,
            }}
          >
            <div className="movie-item-outlayer flex items-center justify-center">
              <Button
                className="  transition duration-300 z-3 w-32 h-32 flex items-center justify-center border-none shadow-none opacity-0 play-button text-red-600 "
                onClick={(event) => handleOpenModal(trailer)(event)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="1.1"
                  stroke="currentColor"
                  class="hover:opacity-60 transition duration-300"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M15.91 11.672a.375.375 0 010 .656l-5.603 3.113a.375.375 0 01-.557-.328V8.887c0-.286.307-.466.557-.327l5.603 3.112z"
                  />
                </svg>
              </Button>
            </div>
          </div>
        }
      >
        <Meta
          className="text-white"
          style={{ height: 78 }}
          title={
            <div className="flex flex-row justify-between items-center text-white">
              <h3>{tenPhim}</h3>
              <button id="like-button" onClick={(event) => handleLike(event)}>
                {isLiked ? likeIcon : unlikeIcon}
              </button>
            </div>
          }
          description={
            <div className="text-white ">
              <div >Chính kịch, hành động</div>
              <p className="space-x-2 flex flex-row"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 4 4" fill="none">
                <path d="M1.73959 0.568909L2.16583 1.41637L3.119 1.55311L2.42929 2.2124L2.59206 3.1438L1.73959 2.70382L0.88712 3.1438L1.04989 2.2124L0.360188 1.55311L1.31336 1.41637L1.73959 0.568909Z" fill="#FBBC05" />
              </svg>{danhGia}</p>
            </div>
          }
        />
      </Card>
    </NavLink>
  );
};
export default ItemMovie;
