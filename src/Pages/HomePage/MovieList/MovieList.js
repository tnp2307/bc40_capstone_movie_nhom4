import React, { useEffect, useState } from "react";
import { movieSer } from "../../../service/movieService";
import ItemMovie from "./ItemMovie";
import { useDispatch } from "react-redux";
import {
  setLoadingOff,
  setLoadingOn,
} from "../../../reduxToolKit/spinnerSlice";
import { useParams } from "react-router-dom";
import MovieGroup from "../MovieGroup";
import { groupInfor } from "../../../reduxToolKit/SelectTypeMovieSlice";
// import required modules
export default function MovieList() {
  let { id } = useParams()
  let index = groupInfor.findIndex
    ((item) => item.gp == id)
  let headerInfor = groupInfor[index]
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    movieSer
      .getMovieList(id)
      .then((res) => {
        setMovies(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className="">
        <MovieGroup title={headerInfor.title} img={headerInfor.img} gp={headerInfor.gp} />
        <div className="relative ">
          <div className="text-white container text-2xl font-semibold ">
            All movie that are in the section
          </div>
        </div>
      </div>
      <div id="movie-list" className="movie-list mx-auto grid grid-cols-2 md:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 ">
        {movies.map((movie) => {
          return <ItemMovie movie={movie} key={movie.maPhim} />
        })}
      </div>
    </div>
  );
}
