import React, { useEffect, useState } from "react";
import { movieSer } from "../../../service/movieService";
import ItemMovie from "./ItemMovie";
import { useDispatch } from "react-redux";
import { setLoadingOff, setLoadingOn } from "../../../reduxToolKit/spinnerSlice";
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

// import required modules
import { Pagination, Navigation } from 'swiper/modules';

export default function MovieListTypeCarousel({ sliceStart, sliceCount, gp }) {
  const [movies, setMovies] = useState([]);
  const [slidesPerView, setSlidesPerView] = useState(1);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setLoadingOn());
    movieSer
      .getMovieList(gp)
      .then((res) => {
        setMovies(res.data.content.slice(sliceStart, sliceCount));
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
      });
  }, []);

  useEffect(() => {
    const handleResize = () => {
      const windowWidth = window.innerWidth;
      let newSlidesPerView = 2;

      if (windowWidth >= 768 && windowWidth < 1024) {
        newSlidesPerView = 3;
      } else if (windowWidth >= 1200&& windowWidth < 1440) {
        newSlidesPerView = 4;
      }else if (windowWidth >= 1440) {
        newSlidesPerView = 5;
      }

      setSlidesPerView(newSlidesPerView);
    };

    window.addEventListener('resize', handleResize);
    handleResize(); // Call initially to set the correct slidesPerView value

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div id="movie-list" className="movie-list mx-auto">
      <Swiper
        slidesPerView={slidesPerView}
        spaceBetween={30}
        pagination={{
          type: 'progressbar',
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {movies.map((movie) => (
          <SwiperSlide key={movie.maPhim}>
            <ItemMovie movie={movie} key={movie.maPhim} />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}
