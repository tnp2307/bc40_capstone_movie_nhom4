import React, { useEffect, useState } from "react";
import ItemMovie from "./ItemMovie";
import { useDispatch } from "react-redux";
import {
  setLoadingOff,
  setLoadingOn,
} from "../../../reduxToolKit/spinnerSlice";
import { movieSerNew } from "../../../service/movieServiceNew";

export default function MovieListNew () {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    movieSerNew
      .getMovieList()
      .then((res) => {
        console.log(res.data.results);
        setMovies(res.data.results);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
      });
  }, []);
  return (
    <div id="movie-list" className="movie-list mx-auto flex flex-wrap justify-start items-start ">
      {movies.map((movie) => {
        return <ItemMovie movie={movie} key={movie.id} />;
      })}
      
    </div>
  );
}
