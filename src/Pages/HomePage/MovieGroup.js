import React from 'react'
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { setMovieList } from '../../reduxToolKit/SelectTypeMovieSlice';
export default function MovieGroup({ title, img, gp }) {
    let src = `/resource/Rectangle-${img}.png`
    let link = `/movie-list/${gp}`
    let dispatch = useDispatch()
    let setGPMovieList = (gpSelect) => {
        dispatch(setMovieList(gpSelect))
    }
    return (
        <div id='group' className=' container pt-10'>
            <NavLink onClick={() => setGPMovieList({ title, img, gp })} className="inline-block transition-all ease-linear" to={link}>
                <div className='text-white flex items-center space-x-2  relative' >
                    <img className='inline-block' width="{40}" height="{40}" src={src} />
                    <span className='text-2xl font-semibold' >{title}</span>
                </div>
            </NavLink>
        </div>
    )
}
