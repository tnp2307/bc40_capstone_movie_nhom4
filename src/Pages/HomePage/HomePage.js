import React from "react";
import MovieList from "./MovieList/MovieList";
import CinemaTab from "./CinemaTab/CinemaTab";
import MovieModal from "../../Component/MovieModal/MovieModal";
import Carousel from "./Carousel/Carousel";
import MovieListNew from "./MovieList/MovieListNew";
import MovieGroup from "./MovieGroup";
import MovieListTypeCarousel from "./MovieList/MovieListTypeCarousel";

export default function HomePage() {
  return (
    <div>
      <Carousel />
      <MovieGroup img="hot" title="Now showing" gp="06" />
      <MovieListTypeCarousel gp="06" sliceStart={0} sliceCount={8} />
      <MovieGroup img="blockbuster" title="Blockbuster" gp="09" />
      <MovieListTypeCarousel gp="09" sliceStart={0} sliceCount={8} />
      {/* <MovieListNew /> */}
      <MovieGroup img="cinema" title="Cinema system" />
      <CinemaTab />
      <MovieModal />
    </div>
  );
}
