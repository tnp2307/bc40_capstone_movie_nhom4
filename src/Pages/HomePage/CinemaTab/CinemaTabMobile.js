import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieSer } from "../../../service/movieService";

import ItemTab from "./ItemTab/ItemTab";
import {
  setLoadingOff,
  setLoadingOn,
} from "../../../reduxToolKit/spinnerSlice";
import { useDispatch } from "react-redux";
import ItemTabMobile from "./ItemTab/ItemTabMobile";

const CinemaTabMobile = () => {
  const dispatch = useDispatch();
  const [heThongRap, setHeThongRap] = useState([]);
  useEffect(() => {
    dispatch(setLoadingOn());
    movieSer
      .getMovieByTheater()
      .then((res) => {
        setHeThongRap(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        dispatch(setLoadingOff());
      });
  }, []);
  const renderCinemaList = () => {
    return heThongRap.map((heThong) => {
      return {
        label: (
          <div className="bg-white flex items-center justify-center cine-system scale-75">
            <img className="h-16 w-16" src={heThong.logo} />
          </div>
        ),
        // key: <imgs /> heThong.maHeThongRap,
        key: heThong.logo,
        children: (
          <Tabs
            id="cine-list"
            style={{ height: 600 }}
            tabPosition="top"
            defaultActiveKey="1"
            key={heThong.maHeThongRap}
            items={heThong.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className=" text-white cine-name flex flex-col justify-evenly items-start py-5">
                    <div className="flex justify-between w-full ">
                      <h2 className="font-bold mr-1">{cumRap.tenCumRap}</h2>
                      
                    </div>
                    <h5>12 Truong Chinh Tan phu</h5>
                    <div className="flex w-16 justify-between">
                        <div>
                          <svg
                            width="15"
                            height="14"
                            viewBox="0 0 15 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M5.9043 11.6787L9.34961 13.6133C9.45215 13.668 9.56152 13.7158 9.66406 13.7432V2.58691L6.28711 0.522461C6.1709 0.447266 6.03418 0.392578 5.9043 0.365234V11.6787ZM0.743164 12.752C0.743164 13.251 1.03027 13.5381 1.52246 13.5381C1.7002 13.5381 1.87109 13.4834 2.09668 13.3604L4.96777 11.8086V0.481445C4.84473 0.542969 4.70801 0.611328 4.58496 0.679688L1.34473 2.53906C0.93457 2.76465 0.743164 3.09277 0.743164 3.55078V12.752ZM10.5938 13.7227C10.6621 13.7021 10.7373 13.6748 10.7988 13.6338L14.3604 11.6104C14.7705 11.3848 14.9619 11.0566 14.9619 10.5986V1.39062C14.9619 0.884766 14.6748 0.604492 14.1826 0.604492C14.0049 0.604492 13.834 0.65918 13.6084 0.782227L10.5938 2.45703V13.7227Z"
                              fill="#D4FF00"
                            />
                          </svg>
                        </div>
                        <h5 className="ml-2"> 0.2 km</h5>
                      </div>
                  </div>
                ),
                children: (
                  <div style={{ height: 575 }} id="schedule-infor" className="overflow-y-scroll flex flex-col items-center justify-start">
                    {cumRap.danhSachPhim.slice(0, 9).map((phim) => {
                      return <ItemTabMobile phim={phim} key={phim.maPhim} />;
                    })}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <>
      <Tabs
        id="cine-system"
        className=" pb-5"
        style={{ height: 900 }}
        defaultActiveKey="1"
        tabPosition="top"
        items={renderCinemaList()}
      />
    </>
  );
};
export default CinemaTabMobile;
