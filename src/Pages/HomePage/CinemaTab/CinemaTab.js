import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../Layout/Responsive'
import CinemaTabDesktop from './CinemaTabDesktop'
import CinemaTabMobile from './CinemaTabMobile'
import CinemaTablet from './CinemaTabTablet'


export default function CinemaTab() {
  return (
    <div>
      <Desktop>
        <CinemaTabDesktop/>
      </Desktop>
      <Tablet>
        <CinemaTablet/>
      </Tablet>
      <Mobile>
        <CinemaTabMobile/>
      </Mobile>
    </div>
  )
}
