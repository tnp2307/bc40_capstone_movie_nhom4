import React from 'react'
import ItemTabMobile from './ItemTabMobile'
import ItemTabDesktop from './ItemTabDesktop'
import { Desktop, Mobile, Tablet } from '../../../../Layout/Responsive'
import ItemTabTablet from './ItemTabTablet'

export default function ItemTab() {
  return (
    <div>
      <Desktop>
        <ItemTabDesktop/>
      </Desktop>
      <Tablet>
        <ItemTabTablet/>
      </Tablet>
      <Mobile>
        <ItemTabMobile/>
      </Mobile>
    </div>
  )
}
