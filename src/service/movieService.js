import { https } from "./config";

export const movieSer = {
  getMovieList: (gp) => {
    return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP${gp}`);
  },
  getMovieByTheater: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP09"
    );
  },
  getMovieDetail: (maPhim) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },
  getScheduleByMovie: (maPhim) => {
    return https.get(
      `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`
    );
  },
  getMovieBanner:()=>{
    return https.get(`/api/QuanLyPhim/LayDanhSachBanner`)
  },
  getTheaterList: (maLich) => {
    return https.get(
      `api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLich}`
    );
  },
  postBookTicket: (ticketInfor) => {
    return https.post(`api/QuanLyDatVe/DatVe`, ticketInfor);
  },
};
