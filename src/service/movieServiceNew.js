import { https, httpsNew } from "./config";

export const movieSerNew = {
  getMovieList: () => {
    return httpsNew.get("/movie/now_playing?language=en-US&page=1");
  },

  getMovieDetail: (maPhim) => {
    return httpsNew.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },

};
