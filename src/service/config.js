import axios from "axios";
import { localStore } from "./localUserStorage";
export const BASE_URL = "https://movienew.cybersoft.edu.vn/";
export const NEW_URL = 'https://api.themoviedb.org/3'
export const TokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U";
export const configHeader = () => {
  return {
    TokenCybersoft: TokenCybersoft,
    Authorization: 'Bearer ' + localStore.get()?.accessToken
  };
};
export const configHeaderNew = () => {
  return {
    accept: 'application/json',
    Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmZDQ1YTM4MGM1OTVmMzY4YTNmMDE1OWZiYzQ2ZDVmMCIsInN1YiI6IjY0YTgwNTM3OTY1MjIwMDExZGYwOTQzZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.pN6WPO7ZCWLYFk5kUAZGVsSSriCfqHUDbGiG7dwC5gk'
  };
};
export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeader(),
});
export const httpsNew = axios.create({
  baseURL: NEW_URL,
  headers: configHeaderNew(),
});
